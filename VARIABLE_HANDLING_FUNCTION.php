
<?php

//floatval

echo "floatable example: " . "<br>";

$ab="7.34bitm15.23" ;

echo floatval($ab)

?>

</br>

<?php

//VAR_DUMP()

echo "var dump example: " . "<br>";

$var="bitm";

var_dump($var);

?>

</br>

<?php

//empty

echo "empty example: " . "<br>";

$a="" ;

echo empty($a);

$b="10";

echo empty($b);

?>

</br>

<?php
//array

echo "array example: " . "<br>";

$yes =array('rifat','hossen');

echo is_array($yes);

echo "<br>";

$no =array("this is a array");

echo is_array($no);
?>

</br>

<?php
//is_null
echo "null example: " . "<br>";

$yes =Null;

echo is_null($yes);

$no ="rifat"; //Its not null

echo is_null($no);

$val =10; //its not null

echo is_null($val);

?>

</br>

<?php

echo "isset example: " . "<br>";

$a="rifat";

echo isset($a);  //return 1

$b="10";

echo isset($b); //return 1

$c;

echo isset($c); //return 0

$d= null;

echo isset($d); //return 0

?>

</br>


<?php


echo "print_r example: " . "<br>";

$a= "rifat";

echo print_r($a);

?>

</br>

<?php


echo "serialize example: " . "<br>";

$str= serialize(array("rifat", "bitm"));

echo $str;

?>


</br>

<?php


echo "unserialize example: " . "<br>";

$arr= unserialize("$str");

var_dump($arr);

?>

</br>

<?php

echo "unset example: " . "<br>";

$a= "string";

unset($a);

?>

</br>








